#!/bin/bash
set -eux

g++ -std=c++17 -Wall -save-temps -time greeting.cxx app.cxx -o hello-dyn

g++ -std=c++17 -Wall --static greeting.cxx app.cxx -o hello-sta

size hello-{dyn,sta}

