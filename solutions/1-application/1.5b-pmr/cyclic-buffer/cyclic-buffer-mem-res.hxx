#include <memory_resource>
using std::pmr::memory_resource;

class CyclicBufferResource : public memory_resource {
    void* const storage;
    const size_t storageSize;
    void* const storageEnd;
    void* nextAddress;

    void* add(void* base, size_t offset) const {
        return static_cast<char*>(base) + offset;
    }

public:
    CyclicBufferResource(void* storage, size_t storageSize) noexcept
            : storage{storage},
              storageSize{storageSize},
              storageEnd{add(storage, storageSize)},
              nextAddress{storage} {
    }

    ~CyclicBufferResource() override = default;
    CyclicBufferResource(const CyclicBufferResource&) = delete;
    CyclicBufferResource& operator =(const CyclicBufferResource&) = delete;

protected:
    void* do_allocate(size_t numBytes, size_t _alignment) override {
        printf("alloc: #bytes=%ld\n", numBytes);

        if (add(nextAddress, numBytes) >= storageEnd) {
            nextAddress = storage;
            printf("* alloc: RESET\n");
        }

        void* address = nextAddress;
        nextAddress = add(nextAddress, numBytes);
        printf("** alloc: addr=%ld\n", address);
        return address;
    }

    void do_deallocate(void* ptr, size_t numBytes, size_t _alignment) override {
        //nothing
    }

    bool do_is_equal(const memory_resource& that) const noexcept override {
        return this == &that;
    }

};


