#include "catch2-unit-testing.hxx"
#include "stack-trace.hxx"
#include <sstream>
#include <csignal>

using namespace std;
using namespace std::literals;
using namespace ribomation;


void bizop(int n) {
    if (n % 2 == 0) {
        int* ptr = nullptr;
        *ptr = 42; // *boom*
        cerr << "should not see this: " << *ptr << "\n";
    } else {
        int a = 42, b = 0;
        a /= b; // *boom*
        cerr << "should not see this: " << a << "\n";
    }
}

void invoke_bizop(int n) {
    bizop(n);
}

void invoke_crasher(int n) {
    invoke_bizop(n);
}


TEST_CASE("recover from a null-pointer error", "[crash]") {
    CrashException::init({SIGSEGV});
    try {
        invoke_crasher(42);
    } catch (const CrashException& err) {
        auto buf = ostringstream{};
        buf << err << "\n";

        auto expected = "ApplicationException: Segmentation fault\n"
                        "1) bizop(int)\n"
                        "2) invoke_bizop(int)\n"
                        "3) invoke_crasher(int)"s;
        REQUIRE_THAT(buf.str(), Catch::Contains(expected));
    }
}

TEST_CASE("recover from a div-by-zero error", "[crash]") {
    CrashException::init({SIGFPE});
    try {
        invoke_crasher(17);
    } catch (const CrashException& err) {
        auto buf = ostringstream{};
        buf << err << "\n";

        auto expected = "ApplicationException: Floating point exception\n"
                        "1) bizop(int)\n"
                        "2) invoke_bizop(int)\n"
                        "3) invoke_crasher(int)"s;
        REQUIRE_THAT(buf.str(), Catch::Contains(expected));
    }
}

