#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

using namespace std;

void runPS(const string& filename) {
    int fd = open(filename.data(), O_WRONLY | O_TRUNC | O_CREAT, 0644);
    if (fd < 0) {
        cerr << "open() failed: " << strerror(errno) << "\n";
        exit(1);
    }

    if (dup2(fd, STDOUT_FILENO) < 0) {
        cerr << "dup2() failed: " << strerror(errno) << "\n";
        exit(1);
    }
    close(fd);

    execl("/bin/ps", "ps", "-ef", NULL);
    cerr << "execl() failed: " << strerror(errno) << "\n";
    exit(1);
}

int main(int argc, char** argv) {
    string filename = (argc == 1) ? "../ps-output.txt" : argv[1];

    if (auto rc = fork(); rc == 0) {
        runPS(filename);
    } else if (rc == -1) {
        cerr << "fork() failed: " << strerror(errno) << "\n";
        exit(2);
    }

    wait(nullptr);
    cout << "output file: " << filename << endl;

    return 0;
}
