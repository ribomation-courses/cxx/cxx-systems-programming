#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <cstring>
#include <cerrno>

#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

using namespace std;
using namespace std::literals;


string type(struct dirent* entry) {
#ifdef _DIRENT_HAVE_D_TYPE
    switch (entry->d_type) {
        case DT_BLK:
            return "Block device";
        case DT_CHR:
            return "Char device";
        case DT_DIR:
            return "Directory";
        case DT_FIFO:
            return "Fifo";
        case DT_LNK:
            return "Sym. link";
        case DT_REG:
            return "Normal file";
        case DT_SOCK:
            return "Socket";
        case DT_UNKNOWN:
            return "Unknown";
    }
#endif
    return "DT_* not supported";
}

bool isDir(struct dirent* entry) {
#ifdef _DIRENT_HAVE_D_TYPE
    return entry->d_type == DT_DIR;
#else
    return false;
#endif
}

string modificationTime(struct stat& info) {
    char buf[128];
    strftime(buf, sizeof(buf), "%F %T", localtime(&info.st_mtime));
    return {buf, strlen(buf)};
}

struct stat metadata(const string& path) {
    struct stat info{};
    lstat(path.c_str(), &info);
    return info;
}

void listDir(const string& path) {
    DIR* dir = opendir(path.c_str());
    if (dir == nullptr) {
        throw invalid_argument("failed to open dir '" + path + "': " + strerror(errno));
    }

    cout << "DIR: " << path << endl;
    for (struct dirent* entry; (entry = readdir(dir)) != nullptr;) {
        string filename = entry->d_name;
        if (isDir(entry)) { filename += "/"; }
        struct stat info = metadata(entry->d_name);
        cout << "  "
             << setw(20) << left << filename << " "
             << setw(12) << left << type(entry) << " "
             << setw(8) << right << info.st_size << " bytes "
             << "[" << modificationTime(info) << "] "
             << endl;
    }
    closedir(dir);
}

int main(int argc, char** argv) {
    cout.imbue(locale{"en_US.UTF8"});

    if (argc == 1) {
        listDir(".");
    } else {
        for_each(argv + 1, argv + argc, [](const string& path) {
            listDir(path);
        });
    }

    return 0;
}
