#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <initializer_list>
#include <stdexcept>
#include <system_error>
#include <csignal>

namespace ribomation {
    using std::ostream;
    using std::string;
    using std::string_view;
    using std::vector;
    using std::runtime_error;
    using std::error_condition;
    using std::system_category;
    using std::cerr;
    using std::initializer_list;
    using namespace std::string_literals;

    auto demangle(const char* mangledName) -> string;
    auto extractName(const char* backtraceInfo) -> string;
    auto createBacktrace(unsigned frameOffset = 0) -> vector<string>;
    void registerSignal(int signo, sighandler_t handler);
    void crashHandler(int signo);

    struct StackTrace {
        const vector<string> callStack;
        explicit StackTrace(unsigned frameOffset = 0);
    };

    struct ApplicationException : runtime_error {
        const StackTrace stackTrace;

        explicit ApplicationException(const string& msg, unsigned frameOffset = 3);
        void printStackTrace(ostream& os = cerr) const;
    };

    struct SystemException : ApplicationException {
        const error_condition systemError;

        explicit SystemException(const string& msg);
        static string fmt(const string& msg);
    };

    struct CrashException : ApplicationException {
        const int code;
        const string message;

        explicit CrashException(int signo);
        static void init(initializer_list<int> signoList);
    };

    auto operator <<(ostream& os, const ApplicationException& err) -> ostream&;

}

