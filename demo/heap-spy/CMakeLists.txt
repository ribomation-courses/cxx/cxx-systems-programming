cmake_minimum_required(VERSION 3.8)
project(object_pool)

set(CMAKE_C_STANDARD 11)
add_compile_options(-Wall -Wextra -Wpedantic -Wfatal-errors -Wno-unused-variable)

add_executable(alloc-spy alloc-spy.c)
add_executable(heap-overflow heap-overflow.c)
add_executable(heap-spy heap-spy.c)
add_executable(stack-mem stack-mem.c)
