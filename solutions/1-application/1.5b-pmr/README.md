# Using std::pmr
## Prerequisites
You need a C++17 PMR compatible compiler to build these programs.

`GNU C++ 9.1` or later, will do.

## Compile

    mkdir bld && cd bld
    cmake ..
    cmake --build .

## Comparing Allocations

    ./std-allocation
    ./pmr-allocation

## Usage of Monotonic Memory Resource

    ./vector

## Impl & Usage of Cyclic Memory Resource

    ./using-cyclic-mem-res
