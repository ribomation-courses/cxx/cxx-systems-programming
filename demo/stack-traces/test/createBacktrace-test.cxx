#include "catch2-unit-testing.hxx"
#include "stack-trace.hxx"

using namespace std::literals;
using namespace ribomation;


TEST_CASE("createBacktrace of nothing, should return at least libc main()", "[create]") {
    auto actual   = createBacktrace();

    auto expected = vector<string>{
            "main()"s,
            "__libc_start_main()"s,
            "_start()"s
    };
    REQUIRE_THAT(actual, Catch::Contains(expected));
}
