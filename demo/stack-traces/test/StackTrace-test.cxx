#include "catch2-unit-testing.hxx"
#include "stack-trace.hxx"

using namespace std::literals;
using namespace ribomation;

vector<string> backTrace{};

int fun2(int n) {
    backTrace = createBacktrace(1);
    return 2 * n;
}

int fun1(int n) {
    return fun2(n) + 42;
}

TEST_CASE("createBacktrace of fun1 -> fun2, should return a trace", "[create]") {
    backTrace.clear();
    fun1(17);

    auto expected = vector<string>{
            "fun2(int)"s,
            "fun1(int)"s,
            "Catch::TestInvokerAsFunction::invoke() const"s
    };
    REQUIRE_THAT(backTrace, Catch::Contains(expected));
}


TEST_CASE("StackTrace{}, should produce a decent trace", "[stack-trace]") {
    auto actual = StackTrace{1};

    auto expected = vector<string>{
            "ribomation::StackTrace::StackTrace(unsigned int)"s,
            "main()"s,
            "__libc_start_main()"s,
            "_start()"s
    };
    REQUIRE_THAT(actual.callStack, Catch::Contains(expected));
}
