//COMPILE: g++ --std=c++11 -Wall -g uptime.cpp -o my-uptime
//RUN    : ./my-uptime

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdexcept>
#include <chrono>
#include <sys/sysinfo.h>

using namespace std;
using namespace std::literals;
using namespace std::chrono;

string toTimeString(long seconds) {
    const long    minute = 60;
    const long    hour   = 60 * minute;
    const long    day    = 24 * hour;
    ostringstream buf;
    buf << setw(2) << setfill('0')
        << (seconds % day) / hour << ':'
        << (seconds % hour) / minute << ':'
        << seconds % minute;
    return buf.str();
}

int numCPUs() {
    ifstream cpuinfo("/proc/cpuinfo");
    if (!cpuinfo) throw runtime_error("Cannot open /proc/cpuinfo");

    int    cpuCount = 0;
    string line;
    while (getline(cpuinfo, line)) {
        if (line.find("processor") != string::npos) cpuCount++;
    }

    return cpuCount;
}

int numCPUsAlt() {
    return get_nprocs();
}

int main() {
    ifstream uptime("/proc/uptime");
    if (!uptime) throw runtime_error("Cannot open /proc/uptime");

    double boot, idle;
    uptime >> boot >> idle;
    idle /= numCPUsAlt();

    cout << "Time since boot: " << toTimeString(boot) << endl;
    cout << "Idle time: " << toTimeString(idle) << endl;

    return 0;
}
