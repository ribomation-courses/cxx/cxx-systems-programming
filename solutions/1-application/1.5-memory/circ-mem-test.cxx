#include <iostream>
#include <iomanip>
#include <string>
#include "circular-memory.hxx"

using namespace std;
using namespace ribomation::memory;

int main(int argc, char** argv) {
    auto n = (argc == 1) ? 100U : stoi(argv[1]);

    auto mem = CircularMemory<60 * sizeof(int)>{};
    for (auto k = 1U; k <= n; ++k) {
        auto* ptr1  = new (mem.alloc(sizeof(short))) short{static_cast<short>(k % 42)};
        auto* ptr2 = new (mem.alloc(sizeof(long))) long{k * 4242};
        auto* ptr3 = new (mem.alloc(sizeof(long double))) long double{k * 33.333};

        cout << setw(4)  << *ptr1 << " @ " << reinterpret_cast<unsigned long>(ptr1)
             << setw(10) << *ptr2 << " @ " << reinterpret_cast<unsigned long>(ptr2)
             << setw(10) << fixed << setprecision(3)
                         << *ptr3 << " @ " << reinterpret_cast<unsigned long>(ptr3)
             << endl;
    }

    return 0;
}
