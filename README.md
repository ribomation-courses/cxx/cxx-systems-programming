Linux Systems Programming using C++, 3 days
====

Welcome to this course.
The syllabus can be find at
[cxx/cxx-systems-programming](https://www.ribomation.com/courses/cxx/cxx-systems-programming.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs


Installation Instructions
====

In order to do the programming exercises of the course, you need to have access to a Linux system, preferably Ubuntu. Go for one of the listed solutions below.

* **Already have Linux installed on your laptop**<br/>
Then you are ready for the course. If it is not Ubuntu, then there might be some differences, but as long as you can handle it and do the translation yourself, there is no problem.
* **Already have access to a remote Linux system**<br/>
Same as above.
* **Is running Windows 10 on your laptop**<br/>
One of the biggest news of the update named "Aniversary Edition" released last summer, was that Windows 10 has support for running native Ubuntu Linux, which is called WSL (Windows Subsystem for Linux). You just have to enabled it. Follow the links below to proceed.
* **Otherwise**<br/>
You have to install VirtualBox and install Ubuntu into a VM. Follow the links below to proceed.

In addition, your need a C/C++ compiler such as GCC/G++ and some way to edit your program code, such as a decent text editor or a full blown IDE. Read more below for suggestions.


Installing WSL @ Windows 10
----
Just open _Microsoft Store_ and search for "Ubuntu". Then choose
Ubuntu 18.04 and install it. It's as simple as that. Here's a video
showing the steps

* https://youtu.be/d-Ys9NBbClI



Installing Ubuntu @ VBox
----

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop<br/>
    <http://www.ubuntu.com/download/desktop>
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
    Set a username and password when asked to and write them down.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>



Installing GCC C++ Compiler and Tools
----

Within a Ubuntu terminal window type the following command to install the
compiler and other tools.

    sudo apt install g++-8 cmake make gdb valgrind git tree

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to logon to Ubuntu.

Mac User
----

If you have a Mac laptop, install a Modern C++ compiler supporting C++ 17
and install JetBrains CLion.

Linux User
----

If you're running a different Linux desktop OS, such as Fedora or similar; use its
package manager to install a compiler supporting C++ 17 and then install  CLion.


Suggestions for editor/IDE
====

You also need to use a text editor or IDE to write your programs. If you are already familiar with tools like Emacs or Eclipse/C++, please go ahead and install them too. On the other hand, if you would like some advise I do recommend to choose one of these two suggestions:

* If you just want a decent text editor, then go for Text Editor (gedit). It's already installed on Ubuntu and you can launch from the start menu in the upper left corner or from a terminal window with the command: `gedit my-file.cpp &`

* If you want to run a good IDE instead, then download and install the trial version of JetBrains CLion from <https://www.jetbrains.com/clion/>
This is my choice of C++ IDE and I will be using it during the course.

* Another, interesting IDE/smart-editor is MS Visual Code, which exists for Linux as well. <https://code.visualstudio.com/>


Running CLion on Windows and use WSL for compilation
----
My favorite C++ IDE is CLion and it works very nicely with WSL; i.e. you install
CLion on Windows but use the compiler resources within WSL. Just follow the 
instructions below. In short, you will run a BASH script that installs the
SSH daemon, then you configure CLion to connect via SSH to WSL.
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)


Installing CLion @ Ubuntu Desktop
====
Just open the Ubuntu Software app, search for `clion` and install it.


Usage of this GIT Repo
====

Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone
this repo.

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone https://gitlab.com/ribomation-courses/cxx/cxx-systems-programming.git gitlab


During the course, solutions will be push:ed to this repo and you can get these by
a git pull operation

    cd ~/cxx-course/gitlab
    git pull



Build Demo and Solution Programs
====

The solutions and demo programs are all using CMake as the build tool. CMake is a cross
plattform generator tool that can generate makefiles and other build tool files. It is also
the project descriptor for JetBrains CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources. What you do need is to
have `cmake`, `make` and `gcc/g++` installed (_see instructions above_). 

Inside a directory with a solution or demo, run the following commands to build the program.
This will create the executable in the build directory.

    mkdir build
    cd build
    cmake -G 'Unix Makefiles' ..
    cmake --build .


Interesting Links
====
Here are some links of interesting stuff.

* [Hello World from Scratch (ACCU 2019)](https://youtu.be/MZo7k_IOCe8)
* [Rich Code for Tiny Computers: A Simple Commodore 64 Game in C++17 (CppCon 2016)](https://youtu.be/zBkNBP00wJE)
* [An Introduction to Custom Allocators (ACCU 2019)](https://youtu.be/IGtKstxNe14)
* [Program Library HOWTO](http://tldp.org/HOWTO/Program-Library-HOWTO/index.html)
* [Filesystem Hierarchy Standard](http://www.pathname.com/fhs/pub/fhs-2.3.html)
* [Compiler Explorer](https://godbolt.org/)
* [C++ Insights](https://cppinsights.io/)
* [C++ Quick Benchmarks](http://quick-bench.com/)
* [Coliru - Online Compiler](https://coliru.stacked-crooked.com/)
* [WandBox - Online Compiler](https://wandbox.org/)
* [Building GCC 9](https://solarianprogrammer.com/2016/10/07/building-gcc-ubuntu-linux/)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

