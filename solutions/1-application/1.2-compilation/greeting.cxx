#include <string>

namespace {
int multiple = 42;
}

std::string greeting() {
    return "Hi there, from a C compilation unit: " + std::to_string(multiple);
}

