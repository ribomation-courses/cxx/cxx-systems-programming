//COMPILE: c99 -g -Wall my-pipe.c -o my-pipe
//RUN    : ./my-pipe <producer> <consumer>

#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void child(const char* exe, int fdToUse, int fdToClose, int fdToRedirect) {
    fprintf(stderr, "child(%s, %d, %d, %d)\n",
            exe, fdToUse, fdToClose, fdToRedirect
    );

    pid_t pid = fork();
    if (pid < 0) {
        perror("my-pipe");
        exit(1);
    }
    if (pid > 0) {
        close(fdToUse);
        return;
    }

    close(fdToClose);
    dup2(fdToUse, fdToRedirect);

    execl(exe, exe, NULL);
    fprintf(stderr, "EXEC failed\n");
    exit(42); //not reachable
}

void mkPipe(const char* producer, const char* consumer) {
    int fd[2];
    if (pipe(fd) < 0) {
        perror("myPipe");
        exit(1);
    }

    child(producer, fd[1], fd[0], STDOUT_FILENO);
    child(consumer, fd[0], fd[1], STDIN_FILENO);
}

int main(int numArgs, char* args[]) {
    char* producer = (numArgs >= 2) ? args[1] : "/bin/ls";
    char* consumer = (numArgs >= 3) ? args[2] : "/usr/bin/wc";

    mkPipe(producer, consumer);
    wait(NULL);
    wait(NULL);

    return 0;
}

