#include <iostream>
#include <string>
#include <cerrno>
#include <cstring>
#include <cstdio>

using namespace std;
using namespace std::literals;


int main(int argc, char** argv) {
    if (argc != 3) {
        cerr << "usage: " << argv[0] << " <from> <to>" << endl;
        return 1;
    }

    auto from = string{argv[1]};
    auto to   = string{argv[2]};
    auto rc   = rename(from.data(), to.data());
    if (rc != 0) {
        cerr << "failed to rename/move " << from << " -> " << to << ": " << strerror(errno) << endl;
        return 2;
    }
    cout << "moved " << from << " -> " << to << endl;

    return 0;
}
