#include <iostream>
#include <string>
#include <string_view>
#include <stdexcept>

#include <cstring>
#include <cerrno>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>

using namespace std;
using namespace std::literals;

unsigned fileSize(const string& name);
unsigned count(const string& phrase, string_view haystack);
void     load(const string& name, unsigned size, char* storage);
char*    mkShm(unsigned size, string name = "/storage_cxx"s);


int main(int argc, char** argv) {
    auto filename   = "./shakespeare.txt"s;
    auto phrase     = "Hamlet"s;
    auto numWorkers = 10;

    for (auto k = 1; k < argc; ++k) {
        string_view arg{argv[k]};
        if (arg == "-f") {
            filename = argv[++k];
        } else if (arg == "-p") {
            phrase = argv[++k];
        } else if (arg == "-w") {
            numWorkers = stoi(argv[++k]);
        } else {
            cerr << "usage: " << argv[0] << " [-p <phrase>] [-f <file>]\n";
            return 1;
        }
    }

    if (numWorkers < 1) {
        throw invalid_argument("numWorkers must be > 0");
    }

    const auto size    = fileSize(filename);
    const auto shmSize = size + numWorkers * sizeof(unsigned);
    const auto shm     = mkShm(shmSize);
    load(filename, size, shm);

    const auto  chunkSize = size / numWorkers;
    unsigned*   results   = reinterpret_cast<unsigned int*>(shm + size);
    for (auto k = 0; k < numWorkers; ++k) {
        if (fork() == 0) {
            string_view chunk{shm + k * chunkSize, chunkSize};
            results[k] = count(phrase, chunk);
            exit(0);
        }
    }
    for (auto k = 0; k < numWorkers; ++k) { wait(0); }

    unsigned  totalCount = 0;
    for (auto k          = 0; k < numWorkers; ++k) {
        totalCount += results[k];
        cout << "child-" << k << ": count=" << results[k] << endl;
    }
    cout << "phrase '" << phrase << "' occurs " << totalCount
         << " times in '" << filename << "'" << endl;

    return 0;
}


unsigned fileSize(const string& name) {
    struct stat data{};
    if (stat(name.c_str(), &data) < 0)
        throw invalid_argument("stat: "s + strerror(errno) + " '"s + name + "'"s);
    return static_cast<unsigned>(data.st_size);
}

void load(const string& name, unsigned size, char* storage) {
    int fd = open(name.c_str(), O_RDONLY);
    if (fd < 0)
        throw invalid_argument("open: "s + strerror(errno));

    auto start = storage;
    do {
        auto bytes = read(fd, start, size);
        size -= bytes;
        start += bytes;
    } while (size > 0);
}

char* mkShm(unsigned size, string name) {
    int    pageSz = getpagesize();
    size_t shmSz  = (1 + (size / pageSz)) * pageSz;

    int shmFD = shm_open(name.c_str(), O_CREAT | O_TRUNC | O_RDWR, 0600);
    if (shmFD < 0) throw runtime_error("shm_open: "s + strerror(errno));

    if (ftruncate(shmFD, shmSz) < 0) throw runtime_error("ftruncate: "s + strerror(errno));

    void* shm = mmap(0, shmSz, PROT_READ | PROT_WRITE, MAP_SHARED, shmFD, 0);
    if (shm == MAP_FAILED) throw runtime_error("mmap: "s + strerror(errno));
    close(shmFD);

    return reinterpret_cast<char*>(shm);
}

unsigned count(const string& phrase, string_view haystack) {
    const auto N     = phrase.size();
    unsigned   count = 0;
    for (auto  start = 0UL; (start = haystack.find(phrase, start)) != string_view::npos; start += N) {
        ++count;
    }
    return count;
}

