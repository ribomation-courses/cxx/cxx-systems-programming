#include <iostream>
#include <string>
#include <cerrno>
#include <cstring>
#include <unistd.h>

using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    if (argc != 2) {
        cerr << "usage: " << argv[0] << " <file>" << endl;
        return 1;
    }

    auto filename = string{argv[1]};
    auto rc       = unlink(filename.data());
    if (rc != 0) {
        cerr << "cannot unlink " << filename << ": " << strerror(errno) << endl;
        return 2;
    }
    cout << "deleted " << filename << endl;

    return 0;
}
