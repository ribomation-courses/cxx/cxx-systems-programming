#include "catch2-unit-testing.hxx"
#include "stack-trace.hxx"

using namespace std::literals;
using namespace ribomation;


TEST_CASE("extractName: passing nullptr, should return an empty string", "[extract]") {
    REQUIRE(extractName(nullptr) == ""s);
}

TEST_CASE("extractName: passing an empty string, should return an empty string", "[extract]") {
    REQUIRE(extractName("") == ""s);
}

TEST_CASE("missing '(', should return an empty string", "[extract]") {
    auto payload = "/mnt/c/Users/..../cmake-build-debug/math-";
    REQUIRE(extractName(payload) == ""s);
}

TEST_CASE("missing '+', should return an empty string", "[extract]") {
    auto payload = "/mnt/c/Users/..../cmake-build-debug/math-error(_ZN10ribomation5log_";
    REQUIRE(extractName(payload) == ""s);
}

TEST_CASE("passing a complete backtraceInfo, should return the (mangled) function name", "[extract]") {
    auto payload = "/mnt/c/Users/..../cmake-build-debug/math-error(_ZN10ribomation5log_2Ed+0xbf) [0x402911]";
    REQUIRE(extractName(payload) == "_ZN10ribomation5log_2Ed"s);
}
