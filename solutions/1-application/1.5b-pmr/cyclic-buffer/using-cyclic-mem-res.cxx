#include <iostream>
#include <algorithm>
#include <vector>
#include "cyclic-buffer-mem-res.hxx"

using std::cout;
using std::endl;
using std::begin;
using std::data;
using std::size;
using std::fill_n;

using std::pmr::vector;

int main(int argc, char** argv) {
    char storage[64] = {};
    fill_n(begin(storage), size(storage) - 1, '.');

    CyclicBufferResource    memory{begin(storage), size(storage)};
    vector<char> v{&memory};
    //v.reserve(28);

    v.push_back('+');
    for (auto ch = 'a'; ch <= 'z'; ++ch) v.push_back(ch);
    v.push_back('!');
    for (auto k=1; k<=5; ++k) v.push_back('=');

    cout << "storage: \n[" << storage << "]\n";

    return 0;
}


