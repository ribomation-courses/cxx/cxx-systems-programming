cmake_minimum_required(VERSION 3.6)
project(2_proc_fs)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -fmax-errors=1")

add_executable(uptime uptime.cpp)
