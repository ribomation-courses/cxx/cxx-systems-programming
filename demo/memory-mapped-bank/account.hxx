#pragma once

#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>
#include <iomanip>
using namespace std;

class Account {
public:
    enum {
        IBAN_SIZE = 128
    };

private: //sizeof(Account): (128+8+8) = 144 bytes
    char   iban[IBAN_SIZE];
    long   balance;
    double rate;

public:
    Account() = default;
    ~Account() = default;
    Account(const Account&) = default;
    Account& operator=(const Account&) = default;

    Account(const string& iban, long balance, double rate) {
        setIban(iban);
        setBalance(balance);
        setRate(rate);
    }

    void setIban(const string& value) {
        memset(iban, '\0', IBAN_SIZE);
        value.copy(iban, min(value.size(), static_cast<size_t>(IBAN_SIZE)));
    }

    const string getIban() const {
        string s{iban, strlen(iban)};
        return s;
    }

    long getBalance() const {
        return balance;
    }

    void setBalance(long balance) {
        Account::balance = balance;
    }

    double getRate() const {
        return rate;
    }

    void setRate(double rate) {
        Account::rate = rate;
    }

    bool operator==(const Account& rhs) const {
        return iban == rhs.iban &&
               balance == rhs.balance &&
               rate == rhs.rate;
    }

    bool operator<(const Account& rhs) const {
        if (iban < rhs.iban)
            return true;
        if (rhs.iban < iban)
            return false;
        if (balance < rhs.balance)
            return true;
        if (rhs.balance < balance)
            return false;
        return rate < rhs.rate;
    }

    friend ostream& operator<<(ostream& os, const Account& account) {
        os << "Account{iban: " << account.getIban()
           << ", balance: SEK " << setw(5) << right << account.getBalance()
           << ", rate: " << setprecision(2) << fixed << account.getRate() << "%}";
        return os;
    }

};

